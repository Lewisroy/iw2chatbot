// Les dépendance que l'on va utiliser
var builder = require('botbuilder');
var restify = require('restify');

// Initialisation du serveur
var server = restify.createServer();
// Le serveur azure aura le choix entre le port en variable d'environnement ou le port 3978 s'il ne trouve pas la variable d'environnement
server.listen(process.env.port || 3978, function() {
    // On log le nom du serveur et l'url de celui-ci
    console.log(`server name:${server.name} | server url: ${server.url}`);
});

// On se connect à un chat avec les credentials de l'api
var connector = new builder.ChatConnector({
appId: process.env.APP_ID,
appPassword:  process.env.APP_PASSWORD
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

// Receive messages from the user and respond by echoing each message back (prefixed with 'You said:')
var bot = new builder.UniversalBot(connector, function (session) {

   
   
    if (session.message.text === "doheavywork") {
        session.sendTyping();
        setTimeout(()  => {
            session.send("Hello there...");
        }, 3000);
       }
        
        bot.on ('typing', function() {
            session.send(`I know your typing something `);
        })
    
    
       
        //session.send(`Tu as dis: ${session.message.text} `);
        //session.send(`Ton message sur ${server.name} fait ${session.message.text.length} caractères`);
        //session.send(`D'autres datas : ${JSON.stringify(session.dialogData)}`);

});

bot.on('conversationUpdate', function (message) {
    if (message.membersAdded) {
        message.membersAdded.forEach(function (identity) {
            if (identity.id === message.address.bot.id) {
                let msg = new builder.Message().address(message.address);
                msg.text('Welcome');
                bot.send(msg);
            }
        });
    }
});
